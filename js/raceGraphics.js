

  function colorRect(topLeftX, topLeftY, boxWidth, boxHeight, fillColor) {
    canvasContext.fillStyle = fillColor;
    canvasContext.fillRect(topLeftX, topLeftY, boxWidth, boxHeight);
  }
  
  function colorCircle(centerX, centerY, radius, fillColor) {
    canvasContext.fillStyle = fillColor;
    canvasContext.beginPath();
    canvasContext.arc(centerX, centerY, radius, 0, Math.PI*2, true);
    canvasContext.fill();
  }

  function drawRotated(what, angle, whereX, whereY) {
    canvasContext.save();
    canvasContext.translate(whereX, whereY);
    canvasContext.rotate(angle);
    canvasContext.drawImage(what,
        -what.width/2, -what.height/2);
    canvasContext.restore();
  }































