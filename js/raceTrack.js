

  // track constants and variables
  const TRACK_W = 40;
  const TRACK_H = 40;
  const TRACK_COLS = 20;
  const TRACK_ROWS = 15;
  const TRACK_ROAD = 0;
  const TRACK_WALL = 1;
  const TRACK_START = 2;
  const TRACK_END = 3;
  const TRACK_TREE = 4;
  const TRACK_FLAG = 5;
  var trackGrid = [
    4,4,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,1, 1,1,1,4,
    4,4,1,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,1,1,
    1,1,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,1,
    1,0,0,0, 0,1,1,1, 1,1,1,1, 1,1,1,1, 0,0,0,1,

    1,0,0,0, 1,1,1,1, 1,4,4,1, 1,1,1,1, 1,0,0,1,
    1,0,0,1, 1,0,0,1, 1,4,4,1, 0,0,0,1, 1,0,0,1,
    1,0,0,1, 0,0,0,0, 1,4,1,0, 0,0,0,0, 1,0,0,1,
    1,0,0,1, 0,0,0,0, 0,1,1,0, 0,0,0,0, 1,0,0,1,

    1,0,0,1, 0,0,0,0, 0,0,1,0, 0,5,0,0, 1,0,0,1,
    1,0,0,1, 0,0,5,0, 0,0,5,0, 0,1,0,0, 1,0,0,1,
    1,0,2,1, 0,0,1,1, 0,0,0,0, 0,1,0,0, 5,0,0,1,
    1,1,1,1, 0,0,1,1, 0,0,0,0, 0,1,0,0, 0,0,0,1,

    1,0,3,0, 0,0,1,1, 1,0,0,0, 1,1,0,0, 0,0,0,1,
    1,0,3,0, 0,0,1,4, 1,1,1,1, 1,1,0,0, 0,0,1,1,
    1,1,1,1, 1,1,1,4, 4,4,4,1, 4,1,1,1, 1,1,1,4
  ];
 
  
  function trackTileToIndex(tileCol, tileRow) {
    return (tileCol + TRACK_COLS * tileRow);
  }

  function isWallAtTileCoord(trackTileCol, trackTileRow) {
    var trackIndex = trackTileToIndex(trackTileCol, trackTileRow);
    var trackTypeHere = trackGrid[ trackIndex ];
    return trackTypeHere == TRACK_WALL
        || trackTypeHere == TRACK_FLAG
        || trackTypeHere == TRACK_TREE;
  }

  function checkForTrackAtPixelCoord(pixelX,pixelY) {
    var tileCol = pixelX / TRACK_W;
    var tileRow = pixelY / TRACK_H;    
    // we'll use Math.floor to round down to the nearest whole number
    tileCol = Math.floor( tileCol );
    tileRow = Math.floor( tileRow );
    // first check whether the car is within any part of the track wall
    if(tileCol < 0 || tileCol >= TRACK_COLS ||
       tileRow < 0 || tileRow >= TRACK_ROWS) {
       return false; // bail out of function to avoid illegal array position usage
    }
    return ! isWallAtTileCoord(tileCol, tileRow);
  }

  
  function drawTracks() {
    for(var eachCol=0; eachCol<TRACK_COLS; eachCol++) {
      for(var eachRow=0; eachRow<TRACK_ROWS; eachRow++) {
        var trackLeftEdgeX = eachCol * TRACK_W;
        var trackTopEdgeY = eachRow * TRACK_H;
        var trackIndex = trackTileToIndex(eachCol, eachRow);
        var trackTypeHere = trackGrid[ trackIndex ];
        var imageForTile;
        switch ( trackTypeHere ) {
          case TRACK_WALL :
            imageForTile = wallPic;
            break;
          case TRACK_FLAG :
            imageForTile = flagPic;
            break;
          case TRACK_END :
          case TRACK_START :
            imageForTile = endPic;
            break;
          case TRACK_TREE :
            imageForTile = treePic;
            break;
          case TRACK_ROAD :
          default :
            imageForTile = roadPic;
            break;
        }
        canvasContext.drawImage(imageForTile, trackLeftEdgeX, trackTopEdgeY);
      }
    }
  }


















