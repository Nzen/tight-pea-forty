

  var carPic = document.createElement( 'img' );
  var roadPic = document.createElement( 'img' );
  var wallPic = document.createElement( 'img' );
  var endPic = document.createElement( 'img' );
  var treePic = document.createElement( 'img' );
  var flagPic = document.createElement( 'img' );
  var picsToLoad = 0;

  function recurUntilAllLoaded() {
    picsToLoad--;
    if ( picsToLoad == 0 ) {
      runGame();
    }
  }

  function loadOneAsset(currAsset, filename) {
    currAsset.onload = recurUntilAllLoaded;
    // ASK function ptr assignment rather than eval?
    currAsset.src = 'raceAssets/'+ filename;
  }

  function loadAllAssets() {
    var assetsForRace = [
      {varName : carPic, fileName: 'player1.png'},
      {varName : roadPic, fileName: 'track_road.png'},
      {varName : wallPic, fileName: 'track_wall.png'},
      {varName : endPic, fileName: 'track_goal.png'},
      {varName : treePic, fileName: 'track_tree.png'},
      {varName : flagPic, fileName: 'track_flag.png'}
    ];
    picsToLoad = assetsForRace.length;
    for ( var picInd = 0; picInd < assetsForRace.length; picInd++ ) {
      loadOneAsset( assetsForRace[picInd].varName,
          assetsForRace[picInd].fileName );
    }
  }




















