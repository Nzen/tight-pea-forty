

  const KC_UP = 38; // kc is keyCode
  const KC_DOWN = 40;
  const KC_LEFT = 37;
  const KC_RIGHT = 39;

  var pressingGas = false;
  var pressingReverse = false;
  var pressingLeft = false;
  var pressingRight = false;

  function initInput() {
    document.addEventListener('keydown', keyPressed);
    document.addEventListener('keyup', keyReleased);
  }  

// document.getElementById('debug').innerHTML = 'press '+ evt.keyCode;

  function keyPressed(evt) {
    toggleKey(evt.keyCode, true);
    evt.preventDefault();
  }

  function keyReleased(evt) {
    toggleKey(evt.keyCode, false);
  }

  function toggleKey( keyCode, become ) {
    switch (keyCode) {
      case KC_UP : {
        pressingGas = become;
        break;
      } case KC_DOWN : {
        pressingReverse = become;
        break;
      } case KC_LEFT : {
        pressingLeft = become;
        break;
      } case KC_RIGHT : {
        pressingRight = become;
        break;
      } 
    }
  }



























