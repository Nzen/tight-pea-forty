

  
  var canvas, canvasContext;

  window.onload = function() {
    canvas = document.getElementById('gameScreen');
    canvasContext = canvas.getContext('2d');
    loadAllAssets();
  }

  function runGame() {
    setRedrawRate();
    initInput();
    initCar();
  }  

  function setRedrawRate() {
    var framesPerSecond = 30;
    setInterval(function() {
        moveEverything();
        drawEverything();
      }, 1000/framesPerSecond);
  }

  function moveEverything() {
    moveCar();
  }
  
  function drawEverything() {
    drawTracks();
    carDraw();
  }











