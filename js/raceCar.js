

  const SPEED_POST_FRICTION = 0.94;
  const FORWARD_ACCELERATION = 0.5;
  const REVERSE_ACCELERATION = 0.2;
  const TURNING_RADIAN = 0.03;
  const MIN_SPEED_TO_TURN = 0.5;

  // variables to keep track of car position
  var carX = 15, carY = 100;
  var carSpeed = 0;
  var carAngle = 0.33;
  
  function initCar() {
    carReset();
  }


  function moveCar() {
    if(Math.abs(carSpeed) > MIN_SPEED_TO_TURN) {
      if(pressingLeft) {
        carAngle -= TURNING_RADIAN*Math.PI;
      }

      if(pressingRight) {
        carAngle += TURNING_RADIAN*Math.PI;
      }
    }
    
    if(pressingGas) {
      carSpeed += FORWARD_ACCELERATION;
    }
    if(pressingReverse) {
      carSpeed -= REVERSE_ACCELERATION;
    }
    
    var nextX = carX + Math.cos(carAngle) * carSpeed; ////
    var nextY = carY + Math.sin(carAngle) * carSpeed; ////
    
    if( checkForTrackAtPixelCoord(nextX,nextY) ) { ////
      carX = nextX; ////
      carY = nextY; ////
    } else { ////
      carSpeed = 0.0; ////
    } ////

    carSpeed *= SPEED_POST_FRICTION;
  }
  
  function carReset() {
    var ind = 0;
    for ( ; ind < trackGrid.length; ind++ )
    {
      if ( trackGrid[ ind ] == TRACK_START )
      {
        trackGrid[ ind ] = TRACK_ROAD;
        break;
      }
    }
    // if ( ind == TRACK_ROWS ) le sigh, didn't put one
    var tileRow = Math.floor( ind / TRACK_COLS );
    var tileCol = ind % TRACK_COLS;
    carX = tileCol * TRACK_W + 0.5 * TRACK_W;
    carY = tileRow * TRACK_H + 0.5 * TRACK_H;
    carAngle = (-0.5 * Math.PI);
  }


  function carDraw(){
  	drawRotated(carPic, carAngle, carX, carY);
  }




























