
var canvas;
var visibleArea;
var ballXx = 400, ballYy = 300;
var ballSpeedXx = 4, ballSpeedYy = 4, aiPaddleSpeed = 0;
const aiPaddleMaxSpeed = 7;
const paddleWidth = 10, paddleHeight = 100, paddleGap = 5;
var userPaddlePosYy = 250, aiPaddlePosYy = 350;
var userScore = 0, aiScore = 0, winningScore = 3;
var gameMode = 'play';

window.onload = function wol() {

	canvas = document.getElementById( 'gameScreen' );
	visibleArea = canvas.getContext( '2d' );
	canvas.addEventListener( 'mousemove', function mmel( evt ) {
		userPaddlePosYy = mousePosition( evt ).y - ( paddleHeight /2 );
	} );
	canvas.addEventListener( 'mouseup', function muel( evt ) {
		if ( gameMode == 'won' ) {
			gameMode = 'play';
			userScore = 0;
			aiScore = 0;
		}
	} );
	var framesPerSecond = 30;
	setInterval( function glBody() {
				updatePositions();
				refresh();
			},
			1000 /framesPerSecond
	);
}

function refresh() {
	// background
	visibleArea.fillStyle = 'gray';
	visibleArea.fillRect( 0,0, canvas.width, canvas.height );
	if ( gameMode == 'won' ) {
		drawWinScreen();
	} else if ( gameMode == 'play' ) {
		drawGameState();
	}
}

function drawWinScreen() {
	visibleArea.fillStyle = 'black';
	visibleArea.font = 'bold 30px monospace';
	var whoWon;
	if ( userScore > winningScore ) {
		whoWon = 'You'
	} else {
		whoWon = 'AI';
	}
	visibleArea.fillText( whoWon +' won\nClick to restart',
			canvas.width /3, canvas.height /3 );
}

function drawGameState() {
	// divider
	visibleArea.setLineDash( [20, 10] );
	visibleArea.beginPath();
	visibleArea.lineWidth = '3';
	visibleArea.strokeStyle = 'white';
	visibleArea.moveTo(canvas.width /2, 0);
	visibleArea.lineTo(canvas.width /2, canvas.height);
	visibleArea.stroke();

	// score
	visibleArea.fillStyle = 'black';
	visibleArea.font = 'bold 30px monospace';
	visibleArea.fillText( 'User '+ userScore +' AI '+ aiScore,
			canvas.width /2, canvas.height /3 );

	// ball
	visibleArea.fillStyle = 'navy';
	visibleArea.beginPath();
	visibleArea.arc( ballXx, ballYy, 10, 0, Math.PI *2, true );
	visibleArea.fill();

	// player paddle
	visibleArea.beginPath();
	visibleArea.fillRect( paddleGap, userPaddlePosYy,
			paddleWidth, paddleHeight );

	// AI paddle
	visibleArea.beginPath();
	visibleArea.fillRect( canvas.width -paddleGap -paddleWidth, aiPaddlePosYy,
			paddleWidth, paddleHeight );
}	

function ballTouchedPlayerWall() {
	return ( ballYy < userPaddlePosYy
				&& ballXx < paddleGap )
			|| ( ballYy > userPaddlePosYy + paddleHeight
				&& ballXx < paddleGap );
}

function ballTouchedAiWall() {
	return ( ballYy < aiPaddlePosYy
				&& ballXx > canvas.width - paddleGap )
			|| ( ballYy > aiPaddlePosYy + paddleHeight
				&& ballXx > canvas.width - paddleGap );
}

function ballTouchedPlayerPaddle() {
	return ballXx < paddleGap + paddleWidth
			&& ballYy > userPaddlePosYy
			&& ballYy < userPaddlePosYy + paddleHeight;
}

function ballTouchedAiPaddle() {
	return ballXx > canvas.width - paddleGap - paddleWidth
			&& ballYy > aiPaddlePosYy
			&& ballYy < aiPaddlePosYy + paddleHeight;
}

function ballTouchedTopOrBottom() {
	return ballYy < paddleGap
			|| ballYy > canvas.height - paddleGap;
}

function aiChosesPosition() {
	var aiPaddleCenter = aiPaddlePosYy + (paddleHeight /2);
	const stillZone = 35;
	if ( ballYy < aiPaddleCenter - stillZone
			&& Math.abs( aiPaddleSpeed ) < aiPaddleMaxSpeed ) {
		aiPaddleSpeed -= 1;
	} else if ( ballYy > aiPaddleCenter + stillZone
			&& Math.abs(aiPaddleSpeed ) < aiPaddleMaxSpeed ) {
		aiPaddleSpeed += 1;
	} else {
		aiPaddleSpeed = 0;
	}
	aiPaddlePosYy += aiPaddleSpeed;
}

function resetBall() {
		if ( aiScore > winningScore
				|| userScore > winningScore ) {
			gameMode = 'won';
		}
		ballXx = canvas.width /2;
		ballYy = canvas.height /2;
		ballSpeedXx *= -1;
		ballSpeedYy = 3;
}

function updatePositions() {
	if ( gameMode == 'won' ) {
		return;
	}
	ballXx += ballSpeedXx;
	ballYy += ballSpeedYy;
	if ( ballTouchedPlayerWall() ) {
		aiScore += 1;
		resetBall();
	} else if ( ballTouchedAiWall() ) {
		userScore += 1;
		resetBall();
	} else if ( ballTouchedPlayerPaddle()
			|| ballTouchedAiPaddle() ) {
		var paddleYyCenter = 0;
		if ( ballXx < paddleGap + paddleWidth +2 ) {
			// user side
			paddleYyCenter = userPaddlePosYy;
		} else {
			paddleYyCenter = aiPaddlePosYy;
		}
		paddleYyCenter += paddleHeight /2;
		ballSpeedXx *= -1;
		ballSpeedYy = (-1 * ballSpeedYy) - ((paddleYyCenter - ballYy) /5);
	} else if ( ballTouchedTopOrBottom() ) {
		ballSpeedYy *= -1;
	}
	aiChosesPosition();
}

function mousePosition( evt ) {
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	var mouseXx = evt.clientX - rect.left - root.scrollLeft;
	var mouseYy = evt.clientY - rect.top - root.scrollTop;
	return {
		x : mouseXx,
		y : mouseYy
	};
}
