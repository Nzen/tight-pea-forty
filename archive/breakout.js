
var canvas;
var visibleArea;
var ballXx = 200, ballYy = 150;
var ballSpeedXx = 4, ballSpeedYy = 4;
const PADDLE_WIDTH = 100, PADDLE_HEIGHT = 20, PADDLE_GAP = 3;
var userPaddlePosYy = 100, userPaddlePosXx = 100;
const BRICK_WIDTH = 40, BRICK_HEIGHT = 50, BRICK_GAP = 2;
const WALL_COL = 3, WALL_ROW = 2;
var wall = new Array( WALL_COL * WALL_ROW );
var userScore = WALL_COL * WALL_ROW; // , winningScore = 3;
var gameMode = 'play';

window.onload = function wol() {

	canvas = document.getElementById( 'gameScreen' );
	visibleArea = canvas.getContext( '2d' );
	canvas.addEventListener( 'mousemove', function mmel( evt ) {
		userPaddlePosXx = mousePosition( evt ).x - ( PADDLE_WIDTH / 2 );
		ballXx = mousePosition( evt ).x; ballYy = mousePosition( evt ).y; // 4TESTS
	} );
	userPaddlePosYy = canvas.height - PADDLE_HEIGHT - PADDLE_GAP;
	resetWall();
	canvas.addEventListener( 'mouseup', function muel( evt ) {
		if ( gameMode == 'won' ) {
			gameMode = 'play';
			resetBall();
			resetWall();
		}
	} );
	const FRAMES_PER_SECOND = 5;
	setInterval( function glBody() {
				updatePositions();
				refresh();
			},
			1000 / FRAMES_PER_SECOND
	);
}

function mousePosition( evt ) {
	var rect = canvas.getBoundingClientRect();
	var root = document.documentElement;
	return {
		x : evt.clientX - rect.left - root.scrollLeft,
		y : evt.clientY - rect.top - root.scrollTop
	};
}

function refresh() {
	// background
	visibleArea.fillStyle = 'gray';
	visibleArea.fillRect( 0,0, canvas.width, canvas.height );

	if ( gameMode == 'won' ) {
		drawWinScreen();
	} else if ( gameMode == 'play' ) {
		drawGameState();
	}
}

function drawWinScreen() {
	visibleArea.fillStyle = 'white';
	visibleArea.fillText( 'You won, click to restart',
			canvas.width /2, canvas.height /3 );
}

function drawScore() {
	visibleArea.fillStyle = 'black';
	visibleArea.font = 'bold 30px monospace';
	visibleArea.fillText( 'Remain '+ userScore,
			canvas.width /2, canvas.height /3 );
}

function drawBall() {
	visibleArea.fillStyle = 'navy';
	visibleArea.beginPath();
	visibleArea.arc( ballXx, ballYy, 10, 0, Math.PI *2, true );
	visibleArea.fill();
}

function drawPlayerPaddle() {
	visibleArea.beginPath();
	visibleArea.fillRect( userPaddlePosXx, userPaddlePosYy - PADDLE_GAP,
			PADDLE_WIDTH, PADDLE_HEIGHT );
}

function drawRemainingBricks() {
	visibleArea.fillStyle = 'black';
	const visHeight = BRICK_HEIGHT - BRICK_GAP;
	const visWidth = BRICK_WIDTH - BRICK_GAP;
	var brickTop, brickLeft;
	for ( var indCol = 0; indCol < WALL_COL; indCol++ ) {
		for ( var indRow = 0; indRow < WALL_ROW; indRow++ ) {
			if ( brickPresent( indCol, indRow ) ) {
				brickTop = indRow * BRICK_HEIGHT;
				brickLeft = indCol * BRICK_WIDTH;
				visibleArea.beginPath();
				visibleArea.fillRect( brickLeft, brickTop,
						visWidth, visHeight );
			}
		}
	}
}

function drawGameState() {
	// drawScore();
	drawRemainingBricks();
	drawBall();
	drawPlayerPaddle();
}

function winState() {
	return userScore <= 0;
}

function ballTouchedPlayerPaddle() {
	return ( ballYy > canvas.height - PADDLE_HEIGHT - PADDLE_GAP )
			&& ballXx > userPaddlePosXx
			&& ballXx < userPaddlePosXx + PADDLE_WIDTH;
}

function ballTouchedBottomOutsideOfPaddle() {
	return ( ballYy > canvas.height - PADDLE_GAP )
			&& ! ballTouchedPlayerPaddle();
}

function ballTouchedTop() {
	return ballYy < PADDLE_GAP;
}

function ballTouchedSides() {
	return ballXx < PADDLE_GAP
			|| ballXx > canvas.width - PADDLE_GAP;
}

function brickPresent( indCol, indRow ) {
	return wall[ indOfBrick( indRow, indCol ) ];
}

function pixXxDistToCol( brickInd ) {
	var col = brickInd % WALL_COL;
	var left = col * BRICK_WIDTH;
	var right = left + BRICK_WIDTH;
	var closerVal = Math.min( ballXx - left, ballXx - right );
	return {
		dist : closerVal,
		isLeft : ( closerVal == (ballXx - left) )
	};
}

function pixYyDistToRow( brickInd ) {
	var col = brickInd % WALL_ROW;
	var top = col * BRICK_HEIGHT;
	var bottom = top + BRICK_HEIGHT;
	var closerVal = Math.min( ballYy - top, ballYy - bottom );
	return {
		dist : closerVal,
		isTop : ( closerVal == (ballYy - top) )
	};
}

function colOfPixel( xx ) {
	if ( xx < 0 ) {
		return 0;
	} else if ( xx > BRICK_WIDTH * WALL_COL ) {
		return -1; // ASK -1?
	} else {
		return Math.floor( xx / BRICK_WIDTH );
	}
}

function rowOfPixel( yy ) {
	if ( yy < 0 ) {
		return 0;
	} else if ( yy > BRICK_HEIGHT * WALL_ROW ) {
		return -1;
	} else {
		return Math.floor( yy / BRICK_HEIGHT );
	}
}

function indOfBrick( row, col ) {
	if ( row < 0 || col < 0 ) {
		return -1;
	} else {
		return col + ( row * WALL_COL );
	}
}

function indexOfPixels( pixXx, pixYy ) {
	return indOfBrick( rowOfPixel( pixYy ), colOfPixel( pixXx ) );
}

function resetBall() {
		ballXx = canvas.width /2;
		ballYy = canvas.height /2;
		ballSpeedXx *= -1;
		ballSpeedYy = 3;
}

function resetWall() {
	for ( var ind = WALL_COL; ind < wall.length; ind++ ) {
		wall[ ind ] = true;
	}
	userScore = WALL_COL * (WALL_ROW -1);
}

function adjustAngleRelativeToPaddle() {
	var paddleXxCenter = userPaddlePosXx + ( PADDLE_WIDTH /2 );
	ballSpeedXx = ( -1 * ballSpeedXx ) - ((paddleXxCenter - ballXx) /4.5);
	if ( ballSpeedYy > 0 ) {
		ballSpeedYy *= -1;
	}
}

function adjustAngleRelativeToCorner( brickInd, wasLeft, wasTop ) {
	var verticalInd;
	if ( wasTop ) {
		verticalInd = brickInd - WALL_ROW;
	} else {
		verticalInd = brickInd + WALL_ROW;
	}
	var horizontalInd;
	if ( wasLeft ) {
		horizontalInd = brickInd - WALL_COL;
	} else {
		horizontalInd = brickInd + WALL_COL;
	}
	var adjacentVertical;
	if ( verticalInd >= 0 && verticalInd < wall.length ) {
		adjacentVertical = wall[ verticalInd ];
	} else {
		adjacentVertical = false;
	}
	if ( horizontalInd >= 0 && verticalInd < wall.length ) {
		adjacentHorizontal = wall[ verticalInd ];
	} else {
		adjacentHorizontal = false;
	}
	if ( adjacentVertical && adjacentHorizontal ) {
		ballSpeedXx *= -1;
		ballSpeedYy *= -1;
	} else if ( adjacentVertical ) {
		ballSpeedXx *= -1;
	} else if ( adjacentHorizontal ) {
		ballSpeedYy *= -1;
	} else {
		ballSpeedXx *= -1;
		ballSpeedYy *= -1;
	}
}

function adjustAngleRelativeToBrick( brickInd ) {
	var xxVector = pixXxDistToCol( brickInd );
	var yyVector = pixYyDistToRow( brickInd );
	if ( xxVector.dist == yyVector.dist ) {
		adjustAngleRelativeToCorner( brickInd,
				xxVector.isLeft, yyVector.isTop );
	}
	else if ( xxVector.dist < yyVector.dist ) {
		// NOTE bouncing off horizontal
		ballSpeedYy *= -1;
	} else {
		// NOTE bouncing off vertical
		ballSpeedXx *= -1;
	}
}

function updatePositions() {
	if ( gameMode == 'won' ) {
		return;
	} else if ( winState() ) {
		gameMode = 'won';
	}
	ballXx += ballSpeedXx;
	ballYy += ballSpeedYy;
	if ( ballTouchedBottomOutsideOfPaddle() ) {
		resetBall();
		resetWall();
	} else if ( ballTouchedPlayerPaddle() ) {
		adjustAngleRelativeToPaddle();
	} else if ( ballTouchedSides() ) {
		ballSpeedXx *= -1;
	} else if ( ballTouchedTop() ) {
		ballSpeedYy *= -1;
	} else {
		var brickInd = indexOfPixels( ballXx, ballYy );
		if ( brickInd >= 0
				&& brickInd < wall.length
				&& wall[ brickInd ] ) {
			adjustAngleRelativeToBrick( brickInd );
			wall[ brickInd ] = false;
			userScore--;
		}
		// else I'm in the center, moving from momentum
	}
}























